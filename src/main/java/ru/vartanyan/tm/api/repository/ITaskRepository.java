package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @Nullable
    List<Task> findAllByProjectId(@NotNull final String projectId,
                                            @NotNull final String userId);

    void removeAllByProjectId(@NotNull final String projectId,
                              @NotNull final String userId);

    void bindTaskByProjectId(@NotNull final String projectId,
                             @NotNull final String taskId,
                             @NotNull final String userId);

    void unbindTaskFromProject(@NotNull final String projectId,
                               @NotNull final String taskId,
                               @NotNull final String userId);

}
