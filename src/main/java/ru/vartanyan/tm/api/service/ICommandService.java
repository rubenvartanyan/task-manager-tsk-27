package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.List;

public interface ICommandService {

   @Nullable
   AbstractCommand getCommandByName(@NotNull final String name);

   @Nullable
   AbstractCommand getCommandByArg(@NotNull final String arg);

   @Nullable
   Collection<AbstractCommand> getArguments();

   @Nullable
   Collection<AbstractCommand> getCommands();

   @Nullable
   Collection<String> getListArgumentName();

   @Nullable
   Collection<String> getListCommandName();

   void add(@NotNull final AbstractCommand command);

   List<String> getCommandList();

}
