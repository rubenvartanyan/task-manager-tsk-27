package ru.vartanyan.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Domain;
import ru.vartanyan.tm.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataBase64LoadCommand extends AbstractDataCommand{

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-base64-load";
    }

    @Override
    public @Nullable String description() {
        return "load base64 data";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final String base64date = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        @NotNull final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64date);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
