package ru.vartanyan.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.dto.Domain;
import ru.vartanyan.tm.exception.system.NotLoggedInException;

public abstract class AbstractDataCommand extends AbstractCommand {

    protected static final String FILE_BINARY = "./data.bin";
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    @SneakyThrows
    public Domain getDomain() throws Exception {
        final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    @SneakyThrows
    public void setDomain(final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

}
